<?php
namespace Greymen\Utilsco\Classes;

class ApiException extends \Exception
{
    
}

class ActiveCampaign
{
    public $active_campaign;
    private $config;

    public function __construct($ac_url, $ac_key)
    {
        $this->active_campaign = new \ActiveCampaign($ac_url, $ac_key);
    }

    public function call($url, $data)
    {
        $copy = [];

        foreach ($data as $key => $val) {
            $copy[urlencode($key)] = $val instanceof \DateTime ? $val->format('Y/m/d') : $val;
        }

        $response = $this->active_campaign->api($url, $copy);

        if (is_string($response)) {
            throw new ApiException($response);
        } else if ($response->success === 0) {
            throw new ApiException($response->result_message);
        }

        return $response;
    }
}