<?php

namespace Greymen\Utilsco;

use App;
use Greymen\Utilsco\Classes\Helpers;
use Event;

use Greymen\Utilsco\Classes\Signer;
use Backend;
use Cms\Classes\Theme;
use Cms\Classes\Page as CmsPage;
use RainLab\Pages\Classes\Page;
use Rainlab\Pages\Classes\PageList;
use System\Classes\PluginBase;
use System\Classes\SettingsManager;
use Greymen\Utilsco\Models\Settings;
use ToughDeveloper\ImageResizer\Classes\Image;
use Validator;
use RedMarlin\Faq\Models\Category;
use Indikator\Content\Models\NewsCategories;

class Plugin extends PluginBase
{
    // public function __construct()
    // {
    //     dd($this->inlineSVG('assets/images/mm-logo-blue.svg',true));
    // }
    public function pluginDetails()
    {
        return [
            'name'        => 'Greymen utils',
            'description' => 'Some utils for OctoberCMS.',
            'author'      => 'Greymen',
            'icon'        => 'icon-cog',
            'homepage'    => 'https://greymen.co/'
        ];
    }

    public function boot()
    {
        Event::listen('cms.page.init', function ($controller) {
            App::singleton('cms.twig.environment', function ($app) use ($controller) {
                return $controller->getTwig();
            });
        });
        Event::listen('backend.page.beforeDisplay', function ($controller, $action, $params) {
            if (Settings::get('use_repeater_auto_fold', false))
                $controller->addJs('/plugins/greymen/utilsco/assets/js/repeater-auto-fold.js');
            if (Settings::get('use_resize_pane', false)) {
                $controller->addJs('/plugins/greymen/utilsco/assets/js/resize-pane.js');
                $controller->addCss('/plugins/greymen/utilsco/assets/css/resize.pane.css');
            }
            $controller->addCss('/plugins/greymen/utilsco/assets/css/css_language_button.css');
        });

        $Extends = ['\RainLab\Pages\Classes\Page', 'Indikator\Content\Models\Portfolio', 'Indikator\Content\Models\News', 'Indikator\Content\Models\Blog'];
        foreach($Extends as $Extend)
        {
            $this->addMethods($Extend);
        }

        Validator::extend('recaptcha', function($attribute, $value, $parameters) {
            return Components\Recaptcha::validate();
        }, trans('greymen.utilsco::lang.validation.recaptcha.error'));
    }
    public function addMethods($class)
    {
        if( ! class_exists($class) ) return;
        $class::extend(function($model) {
            $model->addDynamicMethod('getCmsPages', function() {
                $theme = Theme::getEditTheme();
                $pageList = CmsPage::listInTheme($theme, true);
                $pages = [];
                foreach ($pageList as $pageObject) {
                    $pages[$pageObject->url] = $pageObject->title . ' (' . $pageObject->url . ')';
                }
                return $pages;
            });

            $model->addDynamicMethod('getStaticPages', function() {
                $theme = Theme::getEditTheme();
                $pageList = new PageList($theme);
                $pages = [];
                foreach($pageList->listPages() as $pageObject) {
                    $pages[$pageObject->url] = $pageObject->title . '(' . $pageObject->url . ')';
                }
                $pages['other'] = 'Add custom link..';
                return $pages;
            });

            $model->addDynamicMethod('getCategoryIdOptions', function() {
                $categories = Category::pluck('title', 'id');
                return $categories;
            });

            $model->addDynamicMethod('getNewsCategoryOptions', function() {
                $categoryList = NewsCategories::pluck('name');
                $categories = [];

                foreach($categoryList as $category) {
                    $categories[$category] = $category;
                }
                $categories['all'] = 'All categories';

                return $categories;
            });

            $model->addDynamicMethod('getAnimationStyleOptions', function() {
                return [
                    'fade' => 'Fade',
                    'fade-up' => 'Fade Up',
                    'fade-down' => 'Fade Down',
                    'fade-left' => 'Fade Left',
                    'fade-right' => 'Fade Right',
                    'fade-up-right' => 'Fade Up Right',
                    'fade-up-left' => 'Fade Up Left',
                    'fade-down-right' => 'Fade Down Right',
                    'fade-down-left' => 'Fade Down Left',
                    'flip-up' => 'Flip Up',
                    'flip-down' => 'Flip Down',
                    'flip-left' => 'Flip Left',
                    'flip-right' => 'Flip Right',
                    'slide-up' => 'Slide Up',
                    'slide-down' => 'Slide Down',
                    'slide-left' => 'Slide Left',
                    'slide-right' => 'Slide Right',
                    'zoom-in' => 'Zoom In',
                    'zoom-in-up' => 'Zoom In Up',
                    'zoom-in-down' => 'Zoom In Down',
                    'zoom-in-left' => 'Zoom In Left',
                    'zoom-in-right' => 'Zoom In Right',
                    'zoom-out' => 'Zoom Out',
                    'zoom-out-up' => 'Zoom Out Up',
                    'zoom-out-down' => 'Zoom Out Down',
                    'zoom-out-left' => 'Zoom Out Left',
                    'zoom-out-right' => 'Zoom Out Right',
                    'none' => 'None',
                ];
            });
        });
    }

    public function registerFormWidgets()
    {
        return [
            'Greymen\Utilsco\FormWidgets\CsRepeater' => [
                'label' => 'CS Repeater',
                'code'  => 'csrepeater'
            ]
        ];
    }

    public function registerComponents()
    {
        return [
            'Greymen\Utilsco\Components\Recaptcha' => 'reCaptcha'
        ];
    }

    public function registerSettings()
    {
        return [
            'definitions' => [
                'label'         => 'greymen.utilsco::lang.plugin.name',
                'description'   => 'greymen.utilsco::lang.plugin.description',
                'icon'          => 'icon-cog',
                'category'      => SettingsManager::CATEGORY_CMS,
                'permissions'   => ['greymen.utilsco.access_definitions'],
                'class'         => 'Greymen\Utilsco\Models\Settings',
                'order'         => 801,
            ]
        ];
    }

    public function registerPageSnippets()
    {
        return $this->registerComponents();
    }


    public function registerMarkupTags()
    {
        return [
            'filters' => [
                'crypt_sign' => function($value) {
                    return Signer::wrap($value);
                },
                'twig' => function ($content, $vars=[]) {
                    $env = App::make('cms.twig.environment');
                    return $env->createTemplate($content)->render($vars);
                },
                'inline' => [$this, 'inlineImage'],
                'colorglyph' => [$this, 'colorGlyph'],
                'to_lines' => [$this, 'toLines'],
                'clean_lines' => [$this, 'cleanLines'],
                'suffix' => [$this, 'suffix'],
                'resizeImage'   => [$this, 'resizeImage'],
            ],
            'functions' => [
                // A static method call, i.e Form::open()
                //                'form_open' => ['October\Rain\Html\Form', 'open'],
                'config_get' => ['October\Rain\Support\Facades\Config', 'get']
            ]
        ];
    }
    public function registerListColumnTypes()
    {
        return [
            // A local method, i.e $this->evalUppercaseListColumn()
            'html' => [$this, 'html'],

            // Using an inline closure
            'loveit' => function($value) { return 'I love '. $value; }
        ];
    }

    private function getSessionFunction()
    {
        return [
            'session' => function ($key = null) {
                return session($key);
            },
        ];
    }

    public function resizeImage($object, $props)
    {
        $class = isset($props['class']) ?  $props['class']: '';
        $alt = isset($props['alt']) ?  $props['alt']: '';
        $extension = isset($props['extension']) ? $props['extension'] : 'png';
        $image = new Image($object);
        $image->resize($props['width'], null, ['sharpen' => 50, 'extension' => $extension, 'mode' => 'crop']);
        // dump($image->resize($props['width'], null, ['sharpen' => 50, 'extension' => $extension, 'mode' => 'crop']));
        // die;
        $placeholder_path = 'assets/images/placeholder.gif';
        $placeholder_image = 'themes/' . Theme::getActiveTheme()->getDirName() . '/' . $placeholder_path;
        $mobile = new Image($object);
        // isset($props['mobile_width']) ? $props['mobile_width'] : $props['width']/2;
        $mobile->resize($props['width']/2);

        //  dump($props);
        $return = '<img src="" data-src="'.$mobile.'" data-src-2x="'.$image.'" class="preloading '.$class.'" alt="'.$alt.'">';
        return $return;
        //    die();
    }

    public function toLines($input, $props = null)
    {
        $ret = "";
        $items = explode("\n", $input);
        $animation = 'fade-up';
        if (isset($props['animation'])) {
            $animation = $props['animation'];
        }
        $delay = 150;
        if (isset($props['delay'])) {
            $delay = $props['delay'] + 0;
        }
        $elem = "h1";
        if (isset($props['element'])) {
            $elem = $props['element'];
        }
        $class = "";
        if (isset($props['class'])) {
            $class = $props['class'];
        }


        //        $ret = explode("<br>", $ret);
        $delay_start = 0;
        foreach ($items as $item) {
            // <h1 class="display-4 mb-4 "  data-aos="fade-right" data-aos-delay="250"></h1>
            $item = str_ireplace('[', '<span>', $item);
            $item = str_ireplace(']', '</span>', $item);
            $ret .= '<' . $elem . ' data-aos="' . $animation . '" class="' . $class . '" data-aos-delay="' . $delay_start . '">' . "$item</$elem>";
            $delay_start += $delay;
        }
        return $ret;
    }

    public function cleanLines($input)
    {
        return Helpers::stripInput($input);
    }

    public  function suffix($input, $min = 1, $max = 10)
    {
        $suffix = rand($min, $max);
        $ret = $input . $suffix;
        return $ret;
    }
    public function colorGlyph($input = null, $class = 'header--point', $replace = ['.'])
    {
        $ret = $input;
        if ($class === null) $class = 'header--point';
        foreach ($replace as $item) {
            $span   = '<span class="' . $class . '">' . $item . '</span>';
            $ret    = str_replace($item, $span, $ret);
        }
        return $ret;
    }

    public function inlineImage($file = null, $theme = true, $base64 = false)
    {
        $image = "";
        if (file_exists(plugins_path().$file)) {
            $path  = plugins_path().$file;
        } else if ($theme) {
            $activeTheme = Theme::getActiveTheme();
            $path  = themes_path() . '/' . $activeTheme->getDirName() . '/' . $file;
            if (!file_exists($path) && !empty($activeTheme->getParentTheme())) {
                $path = themes_path() . '/' . $activeTheme->getParentTheme()->getDirName() . '/' . $file;
            }
        } else {
            $path  = storage_path() . '/app/media/' . $file;
        }
        if (file_exists($path))
            $image =  file_get_contents($path);
        if ($base64) $image = base64_encode($image);
        return $image;
    }

    public function html($value, $column, $record)
    {
        return strtoupper($value);
    }
}
