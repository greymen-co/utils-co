# Greymen utils plugin

This pluging is a collection off extensions we used for our projects. 

## Install

composer require greymen/utilsco-plugin


## Helpers

The class for the helpers is Greymen\Utilsco\Classes.

``Greymen\Utilsco\Classes\Helpers::guid`` Generates guid for unique identifiers

## Components
``recaptcha`` The recaptcha component adds a hidden recaptcha_token input field to the page. Add required|recaptcha to the validation rules and you are ready to go. The key and secret can be configured in the settings.

## Form widgets
``csrepeater`` drop-in replacement for repeater with copy/paste functionality 


## TWIG Extensions

``to_lines`` converts multi line \n input content to configurable elements and can add animation to them when needed
``clean_lines`` converts input content to text only
``inline`` converts images to inline images like SVG 
``colorglyph`` Set colors of SVG images
``config_get`` Gives back config settings
``twig`` Parse variables through Twig


## Console Commands

``utils:clearcache`` Clears cache, needs option to clear image thumbs created by the image scale plugin


## List column types

* html 

``html`` shows the html content

``media_image`` converts the url to an image tag

## Block functions for dropdowns
``getStaticPages`` Shows list of static pages
``
    page:
      label: Page
      type: dropdown
      span: left
      default: fade
      options: getStaticPages
``

``getCmsPages`` Shows list of CMS pages
``
    page:
      label: Page
      type: dropdown
      span: left
      default: fade
      options: getCmsPages
``

``getAnimationStyleOptions`` Shows list of animation styles
``
    animation_style:
      label: Animation style
      type: dropdown
      span: left
      default: fade
``